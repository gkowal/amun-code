
# **The AMUN Code**
## Copyright (C) 2008-2024 Grzegorz Kowal

[![Build Status](https://drone.amuncode.org/api/badges/gkowal/amun-code/status.svg)](https://drone.amuncode.org/gkowal/amun-code)

AMUN is a parallel code to perform numerical simulations in fluid approximation
on uniform or non-uniform (adaptive) meshes. The goal in developing this code is
to create a solid framework for simulations with support for number of numerical
methods which can be selected in an easy way through a parameter file. The
following features are already implemented:

* hydrodynamic and magnetohydrodynamic set of equations (HD and MHD),
* both classical and special relativity cases for the above equations,
* Cartesian coordinate system so far,
* uniform and adaptive mesh generation and update,
* a number of time integration methods, from 2nd to 5th order Runge-Kutta
  methods: Strong Stability Preserving and Embedded (with the error control),
* high order reconstructions: from 2nd to 9th order WENO and MP, both explicit
  and compact methods, the 2nd order TVD interpolation has a number of limiters
  supported,
* Riemann solvers of KEPES-, Roe- and HLL-types (HLL, HLLC, and HLLD),
* standard boundary conditions: periodic, open, reflective, hydrostatic, etc.
* turbulence driving using Alvelius or Ornstein–Uhlenbeck methods,
* viscous and resistive source terms,
* support for passive scalars,
* data stored in an internal XML+binary or the HDF5 format,
* data integrity of the XML+binary format guaranteed by the XXH64 or XXH3 hashes;
* support for Zstandard, LZ4, and LZMA compressions in the XML+binary format,
* support for Deflate, SZIP, Zstandard, and ZFP compressions in the HDF5 format,
* easy and consistend Python interface to read snapshots in both formats,
* MPI/OpenMP parallelization,
* completely written in Fortran 2008,
* simple Makefile or CMake for building the code executable,
* minimum requirements, only Fortran compiler and Python are required to
  prepare, run, and analyze your simulations.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.


Developers
==========

 - Grzegorz Kowal <grzegorz@amuncode.org>


Requirements
============

* Fortran 2003 compiler, tested compilers include:
    - [GNU Fortran](https://gcc.gnu.org/fortran/) version 4.5 or newer,
    - [PGI Community Edition](https://www.pgroup.com/products/community.htm),
      version 18.10 or newer,
    - [Intel Fortran](https://software.intel.com/en-us/fortran-compilers)
      compiler version 9.0 or newer.
    - [NVIDIA HPC](https://developer.nvidia.com/hpc-sdk) compiler version 21.11
      or newer.
* Recommended, although optional, [OpenMPI](https://www.open-mpi.org/) for
  parallel runs, tested with version 1.8 or newer.
* Optional [CMake](https://cmake.org) version 3.16 or newer, for advanced
  compilation option selection.
* Optionally, the XML-binary format compression requires:
  [LZ4 library](https://lz4.github.io),
  [Zstandard library](http://facebook.github.io/zstd/), or
  [LZMA library](https://tukaani.org/xz/)
  [XXHASH library](http://www.xxhash.com/).
* Optional [HDF5 libraries](https://www.hdfgroup.org/solutions/hdf5/), tested
  with version 1.10 or newer. The code now uses the new XML-binary snapshot
  format. However, if you still want to use older HDF5 snapshot format, you
  will need these libraries.
* Deflate compression is natively supported in HDF5 libraries, however,
  optionally these compression formats are supported through filters:
  [SZIP](https://support.hdfgroup.org/doc_resource/SZIP/)
  [HDF5Plugin-Zstandard](https://github.com/gkowal/HDF5Plugin-Zstandard),
  [H5Z-ZFP](https://github.com/LLNL/H5Z-ZFP).


Recommended compilation (using CMake)
=====================================

1. Clone the AMUN source code:
  - from GitLab:
    `git clone https://gitlab.com/gkowal/amun-code.git`
  - from Bitbucket:
    `git clone https://grzegorz_kowal@bitbucket.org/amunteam/amun-code.git`,
  - or unpack the archive downloaded from page
   [Downloads](https://bitbucket.org/amunteam/amun-code/downloads/).
2. Create the build directory, e.g. `mkdir amun-build && cd amun-build`.
3. Call `ccmake <path to amun-code>`, e.g. `ccmake ..`, and press 'c' once.
   Set available options, if necessary. Press 'c' once again, and 'g' to
   generate makefiles.
4. Compile the code using `make`. The executable file **amun.x** should be
   available in a few moments.


Alternative compilation (using `make`)
===========================================

1. Clone the AMUN source code:
  - from GitLab:
    `git clone https://gitlab.com/gkowal/amun-code.git`
  - from Bitbucket:
    `git clone https://grzegorz_kowal@bitbucket.org/amunteam/amun-code.git`,
  - or unpack the archive downloaded from page
   [Downloads](https://bitbucket.org/amunteam/amun-code/downloads/).
2. Go to directory **build/hosts/** and copy file **default** to a new file
   named exactly as your host name, i.e. `cp default $HOSTNAME`.
3. Customize your compiler and compilation options in your new host file.
4. Go up to the directory **build/** and copy file **make.default** to
   **make.config**.
5. Customize compilation time options in **make.config**.
6. Compile sources by typing `make` in directory **build/**. The executable file
   **amun.x** should be created there.


Usage
=====

In order to run some test problems you can simply copy the problem parameter
file from directory **problems/** to the location where you wish to run your
test. Copy the executable file **amun.x** from the **build/** directory compiled
earlier. If you provide option _-i <parameter_file>_, the code will know that
parameters have to be read from file _<parameter_file>_. If you don't provide
this option, the code assumes that the parameters are stored in file
**params.in** in the same director.

In order to run serial version, just type in your terminal:
  `./amun.x -i ./params.in`.

In order to run parallel version (after compiling the code with MPI support),
type in your terminal:
  `mpirun -n N ./amun.x -i ./params.in`,
where N is the number of processors to use.


Reading data
============

By default, the code uses the new XML+binary snapshot data format. Parameter
**snapshot_format** set to either **xml** or **h5** controls which file format
is used.

In order to read the data produced in this format, you will need to install the
Python module AmunPy included in subdirectory **python/amunpy**. Simply go to
this directory and run
  `python ./setup.py install --user`
to install the module in your home directory.

Import the module in your python script using
  `from amunpy import *`,
and then initiate the interface to the XML+binary snapshots using
  `snapshot = AmunXML(<path to the snapshot directory>)`
or to the HDF5 files using
  `snapshot = AmunH5(<path to any HDF5 snapshot file>)`
and read desired variables using function
  `var = snapshot.dataset(<variable>)`.

The function **dataset()** returns the requested variable mapped on the uniform
mesh as a NumPy array.
